package ru.falcon_host.Calculator;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.evgenii.jsevaluator.JsEvaluator;
import com.evgenii.jsevaluator.interfaces.JsCallback;

public class Result extends Activity {
    TextView Display;
    String Text;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);
        Display = (TextView)findViewById(R.id.result);
        Text = getIntent().getExtras().getString("STR");
        JsEvaluator jsEvaluator = new JsEvaluator(this);
        jsEvaluator.evaluate(Text, new JsCallback() {
            @Override
            public void onResult(final String result) {
                Display.setText(Text + "=" + result);
            }
        });
    }
}
