package ru.falcon_host.Calculator;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MyActivity extends Activity {
    TextView Display;
    SharedPreferences sPref;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Display = (TextView)findViewById(R.id.display);
    }
    public void OnPressed(View v) {
        String DisplayText;
        //boolean ActionTrigger = false;
        switch (v.getId()) {
            case R.id.button:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "1");
                break;
            case R.id.button2:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "2");
                break;
            case R.id.button3:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "3");
                break;
            case R.id.button4:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "4");
                break;
            case R.id.button5:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "5");
                break;
            case R.id.button6:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "6");
                break;
            case R.id.button7:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "7");
                break;
            case R.id.button8:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "8");
                break;
            case R.id.button9:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "9");
                break;
            case R.id.button10:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "0");
                break;
            case R.id.button12:
                DisplayText = Display.getText().toString();
                if (DisplayText.length() > 0) {
                    Display.setText(DisplayText.substring(0, DisplayText.length() - 1));
                }
                break;
            case R.id.button13:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "+");
                break;
            case R.id.button14:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "-");
                break;
            case R.id.button15:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "*");
                break;
            case R.id.button16:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "/");
                break;
            case R.id.button11:
                DisplayText = Display.getText().toString();
                Intent intent = new Intent(this, Result.class);
                intent.putExtra("STR", DisplayText);
                startActivity(intent);
                break;
            case R.id.button17:
                Display.setText("");
                break;
            case R.id.button18:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + "(");
                break;
            case R.id.button19:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + ")");
                break;
            case R.id.button20:
                DisplayText = Display.getText().toString();
                Display.setText(DisplayText + ".");
                break;
        }
    }
    public void onStop()
    {
        super.onStop();
        sPref = getSharedPreferences("myPreferens", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("dis", Display.getText().toString());
        ed.commit();
    }
    public void onStart()
    {
        super.onStart();
        sPref = getSharedPreferences("myPreferens", MODE_PRIVATE);
        Display.setText(sPref.getString("dis", ""));
    }
}
